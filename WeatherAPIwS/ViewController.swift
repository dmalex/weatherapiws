//  ViewController.swift
//  WeatherSiteWs
//  Created by Dmitry Alexandrov
//  Read it:
//  https://ioscoachfrank.com/remove-main-storyboard.html
import UIKit

class ViewController: UIViewController, UITextFieldDelegate
{
    var topView: UIView?
    var topLabel: UILabel?
    var inputCity: UITextField?
    var getPredictButton: UIButton?
    var predictTextView: UITextView?
    var predictImageView: UIImageView?
    
    let API_KEY = "385041d6079abf142d96faac54572968"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        addView()
        inputCity!.delegate = self
    }
    
    
    private func setupView() {
        topLabel = {
            let control = UILabel()
            control.font = UIFont.systemFont(ofSize: 18, weight: .bold)
            control.textAlignment = .center
            control.textColor = UIColor.lightGray
            control.text = "Weather forecast"
            control.translatesAutoresizingMaskIntoConstraints = false // required
            return control
        }()
        
        inputCity = {
            let control = UITextField()
            control.font = UIFont.systemFont(ofSize: 16, weight: .bold)
            control.textAlignment = .center
            control.textColor = UIColor.black
            control.backgroundColor = UIColor.white
            control.placeholder = "Enter city"
            control.textAlignment = .center
            control.translatesAutoresizingMaskIntoConstraints = false // required
            control.layer.cornerRadius = 4
            control.clipsToBounds = false
            return control
        }()
        
        getPredictButton = {
            let control = UIButton()
            control.backgroundColor = UIColor.gray
            control.layer.cornerRadius = 8
            control.clipsToBounds = false
            control.translatesAutoresizingMaskIntoConstraints = false // required
            control.setTitle("Get forecast", for: .normal)
            control.addTarget(self, action: #selector(getForecast), for: .touchUpInside)
            control.tag = 1
            return control
        }()
        
        predictTextView = {
            let control = UITextView()
            control.backgroundColor = UIColor.lightGray
            control.font = UIFont.systemFont(ofSize: 18, weight: .bold)
            control.textAlignment = .center
            control.textColor = UIColor.white
            control.text = "Enter city and tap 'Get forecast'."
            control.translatesAutoresizingMaskIntoConstraints = false // required
            control.layer.cornerRadius = 4
            control.clipsToBounds = false
            control.isEditable = false
            return control
        }()

        
        predictImageView = {
            let control = UIImageView()
            control.translatesAutoresizingMaskIntoConstraints = false // required
            control.layer.cornerRadius = 4
            control.clipsToBounds = true
            return control
        }()
    }
    
    
    func addView() {
        view.addSubview(topLabel!)
        view.addSubview(getPredictButton!)
        view.addSubview(inputCity!)
        view.addSubview(predictTextView!)
        view.addSubview(predictImageView!)
        
        topLabel!.topAnchor.constraint(equalTo: view.topAnchor, constant: 70).isActive =  true
        topLabel!.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive =  true
        
        inputCity!.topAnchor.constraint(equalTo: topLabel!.bottomAnchor, constant: 20).isActive = true
        inputCity!.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive =  true
        inputCity!.rightAnchor.constraint(equalTo: view.rightAnchor,constant: -20).isActive = true
        inputCity!.heightAnchor.constraint(equalToConstant: 36).isActive = true
        
        getPredictButton!.topAnchor.constraint(equalTo: inputCity!.bottomAnchor, constant: 20).isActive = true
        getPredictButton!.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        getPredictButton!.heightAnchor.constraint(equalToConstant: 36).isActive = true
        getPredictButton!.widthAnchor.constraint(equalToConstant: 180).isActive = true
        
        predictTextView!.topAnchor.constraint(equalTo: getPredictButton!.bottomAnchor, constant: 20).isActive = true
        predictTextView!.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        predictTextView!.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20).isActive = true
        predictTextView!.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true

        predictImageView!.topAnchor.constraint(equalTo: getPredictButton!.topAnchor, constant: -14).isActive = true
        predictImageView!.leftAnchor.constraint(equalTo: predictTextView!.leftAnchor).isActive = true
    }
    
    
    @objc func getForecast(_ sender: UIButton) {
        var s = inputCity!.text!.trimmingTrailingSpaces()
        s = s.trimmingLeadingSpaces()
        inputCity!.text = s
        if s == "" {
            predictTextView!.text = "Enter city and tap 'Get forecast'."
            self.predictImageView?.imageClear()
            return
        }
        var weather = ""
        
        let url = URL(string: "http://api.weatherstack.com/current?access_key=\(API_KEY)&query=\(s.replacingOccurrences(of: " ", with: "-"))")
        
        if url != nil {
            var request = URLRequest(url: url!)
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "GET"

            let unreserved = "-._~/?"
            let allowed = NSMutableCharacterSet.alphanumeric()
            allowed.addCharacters(in: unreserved)

            // let body = "access_key=\(API_KEY)&query=\(s.replacingOccurrences(of: " ", with: "-"))"
            // request.httpBody = body.data(using: String.Encoding.utf8)
            
            var urlError = false
            let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
                if error == nil {
                    print(url!)
                    let jsonWeatherCity = (try! JSONSerialization.jsonObject(with: data!, options: [])) as AnyObject
                    print(jsonWeatherCity)
                    
                    let decoder = JSONDecoder()
                    do {
                        let city = try decoder.decode(WeatherCity.self, from: data!)
                        print(city)
                        
                        
                        weather += city.request.query + "\n "
                        weather += "Temperature: \(String(city.current.temperature))\n"
                        weather += city.current.weather_descriptions[0]
                        let imageUrl = city.current.weather_icons[0]
                        self.predictImageView?.imageFromUrl(urlString: imageUrl)
                        
                    } catch {
                        self.predictImageView?.imageClear()
                        print("Error in JSON parcing.")
                    }
                    
                    print("Data:", data!)
                } else {
                    urlError = true
                    print("Error occured.")
                }
                
                DispatchQueue.main.async {
                    if urlError == true {
                        self.showError(s)
                    } else {
                        self.predictTextView!.text = weather
                    }
                }
            }
            task.resume()
        } else {
            showError(s)
        }
    }
    
    
    func showError(_ s: String) {
        predictTextView!.text = "Was not able to find weather forecast for \(s). Please try again."
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        inputCity!.resignFirstResponder()
        getForecast(getPredictButton!)
        return true
    }
}


struct Current: Codable {
    var cloudcover: Int // = 0
    var feelslike: Int // = 18
    var humidity: Int // = 34
    var is_day: String // = "yes"
    var observation_time: String // = "10:55 AM"
    var precip: Int // = 0
    var pressure: Int // = 1014
    var temperature: Int // = 18
    var uv_index: Int // = 5
    var visibility: Int // = 10
    var weather_code: Int // = 113
    var weather_descriptions: [String] // = [ "Sunny" ]
    var weather_icons: [String] // = [ "https://assets.weatherstack.com/images/wsymbols01_png_64/wsymbol_0001_sunny.png" ]
    var wind_degree: Int // = 350
    var wind_dir: String // = "N"
    var wind_speed: Int // = 22
}


struct Location: Codable {
    var country: String // = "Italy"
    var lat: String // = "41.900"
    var localtime: String // = "2021-10-15 12:55"
    var localtime_epoch: Int // = 1634302500
    var lon: String // = "12.483"
    var name: String // = "Rome"
    var region: String // = "Lazio"
    var timezone_id: String // = "Europe/Rome"
    var utc_offset: String // = "2.0"
}


struct Request: Codable {
    var language: String // = "en"
    var query: String // = "Rome, Italy"
    var type: String // = "City"
    var unit: String // = "m"
}


struct WeatherCity: Codable {
    var current : Current
    var location : Location
    var request : Request
}


extension String {
    func trimmingTrailingSpaces() -> String {
        var s = self
        while s.hasSuffix(" ") {
            s = "" + s.dropLast()
        }
        return s
    }
    
    func trimmingLeadingSpaces() -> String {
        var s = self
        while s.hasPrefix(" ") {
            s = s.dropFirst() + ""
        }
        return s
    }
}

                        
extension UIImageView {
    public func imageFromUrl(urlString: String) {
        if let url = URL(string: urlString) {
            let session = URLSession.shared
            let request = URLRequest(url: url)
            let task = session.dataTask(with: request){
                (data, response, error) -> Void in
                if error != nil {
                    print(error!.localizedDescription)
                } else {
                    if let imageData = data as NSData? {
                        OperationQueue.main.addOperation({ // уходим в main-поток
                            self.image = UIImage(data: imageData as Data)
                        })
                    }
                }
            }
            task.resume()
        }
    }
    
    public func imageClear() {
        OperationQueue.main.addOperation({ // уходим в main-поток
            self.image = nil
        })
    }
}



/*
{"request":{"type":"City","query":"Vladimir, Russia","language":"en","unit":"m"},"location":{"name":"Vladimir","country":"Russia","region":"Vladimir","lat":"56.143","lon":"40.398","timezone_id":"Europe\/Moscow","localtime":"2021-10-15 13:31","localtime_epoch":1634304660,"utc_offset":"3.0"},"current":{"observation_time":"10:31 AM","temperature":5,"weather_code":266,"weather_icons":["https:\/\/assets.weatherstack.com\/images\/wsymbols01_png_64\/wsymbol_0017_cloudy_with_light_rain.png"],"weather_descriptions":["Light drizzle"],"wind_speed":25,"wind_degree":264,"wind_dir":"W","pressure":1014,"precip":0.5,"humidity":92,"cloudcover":100,"feelslike":1,"uv_index":2,"visibility":2,"is_day":"yes"}}

http://api.weatherstack.com/current?access_key=385041d6079abf142d96faac54572968&query=Rom
{
    current =     {
        cloudcover = 0;
        feelslike = 18;
        humidity = 34;
        "is_day" = yes;
        "observation_time" = "10:55 AM";
        precip = 0;
        pressure = 1014;
        temperature = 18;
        "uv_index" = 5;
        visibility = 10;
        "weather_code" = 113;
        "weather_descriptions" =         (
            Sunny
        );
        "weather_icons" =         (
            "https://assets.weatherstack.com/images/wsymbols01_png_64/wsymbol_0001_sunny.png"
        );
        "wind_degree" = 350;
        "wind_dir" = N;
        "wind_speed" = 22;
    };
    location =     {
        country = Italy;
        lat = "41.900";
        localtime = "2021-10-15 12:55";
        "localtime_epoch" = 1634302500;
        lon = "12.483";
        name = Rome;
        region = Lazio;
        "timezone_id" = "Europe/Rome";
        "utc_offset" = "2.0";
    };
    request =     {
        language = en;
        query = "Rome, Italy";
        type = City;
        unit = m;
    };
}
656 bytes
*/
